# -*- coding: utf-8 -*-

"""read_xml.py
    This file definesone function to read data of an xml file.
"""


def read_xml(file_path):
    """This function read the data of an xml file and return it.
    
    Arguments:
        file_path {str} -- Path to the file
    
    Returns:
        [str] -- Data of the file
    """

    # do some stuff...
    data = file_path
    # have done some stuff!
    return data


# This test determines if the file is imported or directly executed
if __name__ == '__main__':
    # Directly executed, do some tests
    # Try function
    read_xml('file.xml')
