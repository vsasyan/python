# -*- coding: utf-8 -*-

"""geo.py
    This file defines the geo function to;
        - transform XML data in geom;
        - write Shapefile.
"""


def xml_to_geom(xml: str) -> str:
    """This function transforms xml data in geographical data.

    Arguments:
        xml {str} -- The string of the xml file

    Returns:
        str -- The geom
    """

    # do some stuff...
    geom = xml
    # have done some stuff!
    return geom


def write_shapefile(geom: str, file: str) -> bool:
    """This function writes geometries in shapefile.

    Arguments:
        geom {str} -- The geometries in string
        file {str} -- The path of the file

    Returns:
        bool -- True if success
    """

    # do some stuff...
    print(geom)
    print(file)
    # have done some stuff!
    return True


# This test determines if the file is imported or directly executed
if __name__ == '__main__':
    # Directly executed, do some tests
    xml = 'test'
    # Try first function
    geom = xml_to_geom(xml)
    # Try second function
    write_shapefile(geom, 'output.shp')
