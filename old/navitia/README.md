# Navitia

## Introduction

This small project is made to introduce an API. API (as application programming interface), are services made to communicate: you can get information by sending simple Internet requests.

[Navitia](https://www.navitia.io/) is an API about transport data.

## Account and beginning

### Account

Most of the API give you a free development account to test their service.

Create a free account on Navitia: [https://www.navitia.io/register/](https://www.navitia.io/register/)

Then go to [My account](https://www.navitia.io/profile/) page and create a token (if needed) then copy your token.

The token is a string like `aaaaaaaa-42df-42df-42df-12345678aaaa`.


### Playground

To catch in hand the different possibilities of the API, Navitia authors have create the [navitia playground](https://canaltp.github.io/navitia-playground/index.html). This is an graphical interface that allows you to make requests to the API.

In this exercice, we want to:
* convert an address in a location (latitude and longitude) ;
* calculate the best journey between two locations.

Go to the [navitia playground](https://canaltp.github.io/navitia-playground/index.html) page and look at:
* [Journey with coverage page](https://canaltp.github.io/navitia-playground/play.html?request=https%3A%2F%2Fapi.navitia.io%2Fv1%2Fcoverage%2Ffr-idf%2Fjourneys%3Ffrom%3D2.36309%253B48.86703%26to%3D2.32738%253B48.86381%26) :
   * enter your token ;
   * enter the **coverage** `fr-idf` ;
   * enter a from position (ex : `2.36309;48.86703`);
   * enter a to position (ex : `2.32738;48.86381`) ;
   * click **submit**.
* [Auto complet places](https://canaltp.github.io/navitia-playground/play.html?request=https%3A%2F%2Fapi.navitia.io%2Fv1%2Fcoverage%2Ffr-idf%2Fplaces%3Fq%3D5%2520avenue%2520Anatole%2520France%252C%252075007%2520Paris%26) :
   * enter your token ;
   * enter the **coverage** `sandbox` ;
   * enter an address in the **q** parameter (ex : `5 avenue Anatole France, 75007 Paris`) ;
   * click **submit**.

Example of the interface :

![Journey with coverage page](img/playground_1.png)

![Auto complet places](img/playground_2.png)

You can explore the response by clicking on the right :
* `EXT` shows you formatted response ;
* `MAP` shows you geographic data (when there is) ;
* `{ }` shows you the raw data.

Look at `journeys[0]` and `places[0]`.

## A first test

Now go back to Python script editor.

The next step is to call the API from a Python script to be able to use the information in Python.

### Modules

The idea is to send an Internet request to the Navitia API to get data.

We will need to import two Python modules:

* `requests` to make Internet requests;
* `json` to manage data.

Your script must starts by:

```py
# Import modules
import requests
import json
```

The `json` module is already installed with Python, but you *may* need to install the `requests` module.

Inside Spyder or Jupyter run:

```sh
!pip install requests
```

`pip` is the package management system of Python, the command .

### The Request

We need to give credential information, stored in a dictionary (remember the token!):

```py
# Header dictionary (for token)
header_dict = {
    "Authorization" : 'aaaaaaaa-PUT-YOUR-TOKEN-aaaaaaaa'
}
```

Now we need an URL. They have the following pattern: 

```
http://api.navitia.io/v1/{something}
```

We will use the geocoding service named `places` to have the coordinates of the Eiffel Tower from its address:

```py
# URL of the request
url_geocoding = 'http://api.navitia.io/v1/places?q=5 avenue Anatole France, 75007 Paris&type[]=address'
```

Then we can send the request, check that the status code is ok (200 => success) and print the result:

```py
# Make the request
r = requests.get(url_geocoding, headers=header_dict)

if r.status_code == 200:
    # Get returned data of the request
    returned_data = r.text
    # Print returned_data
    print(returned_data)
else:
    print("error: status_code={}".format(r.status_code))
```

You will have the output:

```json
{"links":[],"feed_publishers":[{"url":"http:\/\/bano.openstreetmap.fr\/data\/lisezmoi-bano.txt","id":"bano","license":"ODbL","name":"Base d'Adresses Nationale Ouverte"},{"url":"https:\/\/www.openstreetmap.org\/copyright","id":"osm","license":"ODbL","name":"openstreetmap"}],"context":{"timezone":"Africa\/Abidjan","current_datetime":"20211004T122259"},"places":[{"embedded_type":"address","id":"2.294496;48.858262","quality":0,"name":"5 Avenue Anatole France (Paris)","address":{"name":"5 Avenue Anatole France","house_number":5,"coord":{"lat":"48.858262","lon":"2.294496"},"label":"5 Avenue Anatole France (Paris)","administrative_regions":[{"insee":"","name":"Quartier du Gros-Caillou","level":10,"coord":{"lat":"48.8582992032","lon":"2.30153826529"},"label":"Quartier du Gros-Caillou (75007), Paris 7e Arrondissement, Paris, Île-de-France, France","id":"admin:osm:relation:2188569","zip_code":"75007"},{"insee":"75056","name":"Paris","level":8,"coord":{"lat":"48.8566969","lon":"2.3514616"},"label":"Paris (75000-75116), Île-de-France, France","id":"admin:fr:75056","zip_code":"75000-75116"},{"insee":"75","name":"Paris","level":6,"coord":{"lat":"48.8566969","lon":"2.3514616"},"label":"Paris, Île-de-France, France","id":"admin:osm:relation:71525"},{"insee":"11","name":"Île-de-France","level":4,"coord":{"lat":"48.8566969","lon":"2.3514616"},"label":"Île-de-France, France","id":"admin:osm:relation:8649"},{"insee":"","name":"France","level":2,"coord":{"lat":"48.8566969","lon":"2.3514616"},"label":"France","id":"admin:osm:relation:2202162"},{"insee":"75107","name":"Paris 7e Arrondissement","level":9,"coord":{"lat":"48.8570281","lon":"2.3201953"},"label":"Paris 7e Arrondissement (75007), Paris, Île-de-France, France","id":"admin:osm:relation:9521","zip_code":"75007"}],"id":"2.294496;48.858262"}}],"warnings":[{"message":"This service is under construction. You can help through github.com\/CanalTP\/navitia","id":"beta_endpoint"}]}
```

This output is [JSON](https://en.wikipedia.org/wiki/JSON) formatted text. [JSON](https://en.wikipedia.org/wiki/JSON) (as JavaScript Object Notation) is a format that uses human-readable text to transmit data. You can transmit:

* lists (or arrays) represented by `[element1, element2, ...]`;
* dictionaries represented by `{"key": value}`;
* other values (string or numbers): `"one string"` or a number `42`.

So we made a request to the API, and it sent back data as JSON format.

We can modify the script to transform JSON data to Python object. Then we transform again the data in text with JSON module, but with indentation and print it.

```py
if r.status_code == 200:
    # Get returned data of the request
    returned_data = r.text
    # Transform JSON data to Python object
    data = json.loads(returned_data)
    # We Print data with indentation using json module
    print(json.dumps(data, indent=4))
else:
    print("error: status_code={}".format(r.status_code))
```

Now you should have the output:

```json
{
    "links": [],
    "feed_publishers": [
        {
            "url": "http://bano.openstreetmap.fr/data/lisezmoi-bano.txt",
            "id": "bano",
            "license": "ODbL",
            "name": "Base d'Adresses Nationale Ouverte"
        },
        {
            "url": "https://www.openstreetmap.org/copyright",
            "id": "osm",
            "license": "ODbL",
            "name": "openstreetmap"
        }
    ],
    "context": {
        "timezone": "Africa/Abidjan",
        "current_datetime": "20211004T122431"
    },
    "places": [
        {
            "embedded_type": "address",
            "id": "2.294496;48.858262",
            "quality": 0,
            "name": "5 Avenue Anatole France (Paris)",
            "address": {
                "name": "5 Avenue Anatole France",
                "house_number": 5,
                "coord": {
                    "lat": "48.858262",
                    "lon": "2.294496"
                },
                "label": "5 Avenue Anatole France (Paris)",
                "administrative_regions": [
                    {
                        "insee": "",
                        "name": "Quartier du Gros-Caillou",
                        "level": 10,
                        "coord": {
                            "lat": "48.8582992032",
                            "lon": "2.30153826529"
                        },
                        "label": "Quartier du Gros-Caillou (75007), Paris 7e Arrondissement, Paris, Île-de-France, France",
                        "id": "admin:osm:relation:2188569",
                        "zip_code": "75007"
                    },
                    {
                        "insee": "75056",
                        "name": "Paris",
                        "level": 8,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Paris (75000-75116), Île-de-France, France",
                        "id": "admin:fr:75056",
                        "zip_code": "75000-75116"
                    },
                    {
                        "insee": "75",
                        "name": "Paris",
                        "level": 6,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Paris, Île-de-France, France",
                        "id": "admin:osm:relation:71525"
                    },
                    {
                        "insee": "11",
                        "name": "Île-de-France",
                        "level": 4,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Île-de-France, France",
                        "id": "admin:osm:relation:8649"
                    },
                    {
                        "insee": "",
                        "name": "France",
                        "level": 2,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "France",
                        "id": "admin:osm:relation:2202162"
                    },
                    {
                        "insee": "75107",
                        "name": "Paris 7e Arrondissement",
                        "level": 9,
                        "coord": {
                            "lat": "48.8570281",
                            "lon": "2.3201953"
                        },
                        "label": "Paris 7e Arrondissement (75007), Paris, Île-de-France, France",
                        "id": "admin:osm:relation:9521",
                        "zip_code": "75007"
                    }
                ],
                "id": "2.294496;48.858262"
            }
        }
    ],
    "warnings": [
        {
            "message": "This service is under construction. You can help through github.com/CanalTP/navitia",
            "id": "beta_endpoint"
        }
    ]
}
```

### Process data

So the data sent by the API is a **dictionary**. Remember that a dictionary is a collection of key and value pairs.

The most important *key* is `places`. Its *value* is a **list**. As we give an address to the API, it can send back several places if the address is not enough precise...

Here is fine, there is only one place:

```json
        {
            "embedded_type": "address",
            "id": "2.294496;48.858262",
            "quality": 0,
            "name": "5 Avenue Anatole France (Paris)",
            "address": {
                "name": "5 Avenue Anatole France",
                "house_number": 5,
                "coord": {
                    "lat": "48.858262",
                    "lon": "2.294496"
                },
                "label": "5 Avenue Anatole France (Paris)",
                "administrative_regions": [
                    {
                        "insee": "",
                        "name": "Quartier du Gros-Caillou",
                        "level": 10,
                        "coord": {
                            "lat": "48.8582992032",
                            "lon": "2.30153826529"
                        },
                        "label": "Quartier du Gros-Caillou (75007), Paris 7e Arrondissement, Paris, Île-de-France, France",
                        "id": "admin:osm:relation:2188569",
                        "zip_code": "75007"
                    },
                    {
                        "insee": "75056",
                        "name": "Paris",
                        "level": 8,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Paris (75000-75116), Île-de-France, France",
                        "id": "admin:fr:75056",
                        "zip_code": "75000-75116"
                    },
                    {
                        "insee": "75",
                        "name": "Paris",
                        "level": 6,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Paris, Île-de-France, France",
                        "id": "admin:osm:relation:71525"
                    },
                    {
                        "insee": "11",
                        "name": "Île-de-France",
                        "level": 4,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "Île-de-France, France",
                        "id": "admin:osm:relation:8649"
                    },
                    {
                        "insee": "",
                        "name": "France",
                        "level": 2,
                        "coord": {
                            "lat": "48.8566969",
                            "lon": "2.3514616"
                        },
                        "label": "France",
                        "id": "admin:osm:relation:2202162"
                    },
                    {
                        "insee": "75107",
                        "name": "Paris 7e Arrondissement",
                        "level": 9,
                        "coord": {
                            "lat": "48.8570281",
                            "lon": "2.3201953"
                        },
                        "label": "Paris 7e Arrondissement (75007), Paris, Île-de-France, France",
                        "id": "admin:osm:relation:9521",
                        "zip_code": "75007"
                    }
                ],
                "id": "2.294496;48.858262"
            }
        }
```

The place is also a **dictionary**.

Try to get the coordinates of the place from the `data` python object.

Tips:
* to get the first `place` element it is: `data['places'][0]`;
* look at the last output: what are the other **keys** to get the coordinates?

## Used urls

Each functionality of the API has an URL, in the project, we will use:
* Journey with coverage page: `http://api.navitia.io/v1/places?q={address}&type[]=address`
* Auto complet places: `http://api.navitia.io/v1/journeys?from={from_lon};{from_lat}&to={to_lon};{to_lat}&datetime={datetime}&datetime_represents=arrival`

You can format theses URL patterns with the `format()` function to generate the URL to call.

## Exercice

Make two functions: one to geocode an address in a location (auto complete places functionality), one to calculate the best journey between to locations (journey with coverage page).

### geocoding

`geocoding(address)` function will take an address as parameter and return coordinates.

Exemple:

```py
address = '5 avenue Anatole France, 75007 Paris'
coord = geocoding(address)
print(coord) # Output: {'lat': '48.858261', 'lon': '2.294495'}
```

### best journey

`best_journey(start, end)` function will take two coordinates as parameter (`start` and `end`) and return the **best** proposed journey to arrive at the destination **before 9am next monday**. (The best is the one that starts the latest.)

Exemple:

```py
start = {'lat': '48.8467927', 'lon': '2.3749036'}
end = {'lat': '48.8583736', 'lon': '2.2922926'}
journey = best_journey(start, end)
print(journey)
```

If needed, this function will return the date of the next monday in the good format:

```py
def next_datetime():
    """Return next monday at 9am datetime string.
    Need to import datetime module : import datetime
    May need the `python-dateutil` module: !pip install python-dateutil
    
    Returns:
        [str] -- next monday date: YYYY-MM-DDThh:mm:ss
    """

    now = datetime.datetime.now()
    day = 0 # next monday
    now_9am = now.replace(hour=9,minute=0,second=0,microsecond=0) # at 9am
    next_monday = now_9am + datetime.timedelta(days=(day-now_9am.weekday()+7)%7)
    return next_monday.replace(microsecond=0).isoformat()
```
