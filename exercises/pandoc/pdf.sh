#!/bin/sh

# sudo apt install pandoc texlive-base texinfo tex-common texlive-latex-base texlive-fonts-recommended texlive-lang-french texlive-full

export LANG="fr_FR.UTF-8"

pandoc -N \
    --toc \
    --variable documentclass=article \
    --variable fontsize=12pt \
    --variable papersize=a4paper \
    --variable geometry:"top=3.5cm, bottom=3cm, left=2.5cm, right=2.5cm" \
    --variable dateNumber="$(date +'%d-%m-%Y')" \
    --variable date="$(date +'%-d %B %Y')" \
    --template=pandoc/pdf.template.tex \
    pandoc/*.yml exercise_*.md -o Exercises.pdf
