# Exercises

1) [Numbers, String and Variables](exercise_01.md)
2) [Conditions](exercise_02.md)
3) [Lists and Dictionaries](exercise_03.md)
4) [Loops](exercise_04.md)
5) [Functions](exercise_05.md)
6) [Data](exercise_06.md)
7) [Pandas](exercise_07.md)

You can either read it online (links above) or read the [associated PDF](Exercises.pdf).

The online version is always up to date.
