# Numbers, String and Variables


## Variables types

Create 3 variables (one string, one integer and one float) named `my_string`, `my_integer` and `my_float` containing:

* "hi!"
* 42
* 30.24

Put the good value in the good variable!


## Hello world! 1

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the name of the user,
* then prepare a string pattern to say hello with the name of the user,
* format the pattern with the name of the user,
* and to finish print the formatted string (function [print](https://docs.python.org/3/library/functions.html#print)).


## Calculator 1

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the user 2 numbers (do not forget that it will be a string to convert),
* convert them to integers (or floats),
* make the sum,
* and then show result using a f-string (function [print](https://docs.python.org/3/library/functions.html#print)).


## A and B

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the user 2 integers X and Y (do not forget that it will be a string to convert),
* convert them to integers,
* calculate `a` and `b` to have `X * a + b = Y`,
* and then show `a` and `b` using a f-string (function [print](https://docs.python.org/3/library/functions.html#print)).

You will have to use :

* the `//` operator which give the Quotient of the Euclidean division;
* the `%` operator which give the Remainder of the Euclidean division.


## Corrections

### Variables types

```py
my_string = "hi!"
my_integer = 42
my_float = 30.24
```


### Hello world! 1

```py
# Use input function to ask the user his name
user_entry = input("What is it your name? ")
# Declare a pattern
pattern = "Hello {user}!"
# Replace the user parameter by the value of the user_entry variable in the pattern
hello_string = pattern.format(user=user_entry)
# Print the result
print(hello_string)
```


### Calculator 1

```py
# Use input function to ask the user 2 numbers (string type)
number_1_str = input("First integer? ")
number_2_str = input("Second integer? ")
# Use int function to convert the 2 numbers to the integer type
number_1_int = int(number_1_str)
number_2_int = int(number_2_str)
# Calculate the sum
the_sum = number_1_int + number_2_int
# Print the result
print(f"The sum: {the_sum}")
```


### A and B

```py
# Use input function to ask the user 2 numbers (string type) X and Y
number_x_str = input("Enter X an integer? ")
number_y_str = input("Enter Y an integer? ")
# Use int function to convert the 2 numbers to the integer type
number_x_int = int(number_x_str)
number_y_int = int(number_y_str)
# Calculate a and b thank to python build-in operators
a = number_y_int // number_x_int    # a is the Quotient of the Euclidean division
b = number_y_int % number_x_int     # b is Remainder of the Euclidean division
# Print the result
print(f"a = {a} and b = {b}")
```
