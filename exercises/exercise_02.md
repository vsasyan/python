# Conditions


## Odd or Even

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the user one integer (do not forget that it will be a string to convert),
* convert its to an integer,
* test if it is odd or not,
* and then show a message saying if it is an odd or an even number (function [print](https://docs.python.org/3/library/functions.html#print)).

(Do not forget the `%` operator which give the Remainder of the Euclidean division.)


## Calculator 2

With the function [input](https://docs.python.org/3/library/functions.html#input) :

* ask the user 2 numbers (do not forget that it will be a string to convert),
* convert them to integers (or floats),
* ask the user to enter :
    * `+` if he want to calculate the sum;
    * `-` if he want to calculate the difference;
    * `*` if he want to calculate the product;
    * `/` if he want to divide the first number by the second one;
* make the good operation according to the sign entered,
* and then show the result (function [print](https://docs.python.org/3/library/functions.html#print)).


## Corrections

## Odd or Even

```py
# Use input function to ask the user 1 number (string type)
number_str = input("One integer? ")
# Use int function to convert the number to the integer type
number_int = int(number_str)
# The the number is odd if the Remainder of the Euclidean division is 0
if number_int % 2 == 0:
    number_type = "odd"
else:
    number_type = "even"
# Print the result
print(f"{number_int} is an {number_type} number")
```


## Calculator 2

```py
# Use input function to ask the user 2 numbers (string type)
number_1_str = input("First integer? ")
number_2_str = input("Second integer? ")
# Use int function to convert the 2 numbers to the integer type
number_1_int = int(number_1_str)
number_2_int = int(number_2_str)
# Use input function to ask the user the operation to do (string type)
operation_str = input("What do you want to do (enter +, -, * or /)? ")
# Make the operation according to the sign
if operation_str == "+":
    the_result = number_1_int + number_2_int
elif operation_str == "-":
    the_result = number_1_int - number_2_int
elif operation_str == "*":
    the_result = number_1_int * number_2_int
elif operation_str == "/":
    the_result = number_1_int / number_2_int
else:
    the_result = "Wrong operator!"
# Print the result
print(f"The result: {the_result}")
```
